section .text

extern printf
global strlen


strlen:	

	push ebp

	; push fmt
	; call printf
	; add esp,4



	mov ebp,esp ; Stack frame

	sub esp,4 ; reservo 4 bytes
	pushad

	mov ebx,[ebp+8] ; pointer al string
	mov edx, 0
ciclo:
	inc edx
	inc ebx
	mov cl,[ebx]	
	cmp cl,0
	jnz ciclo
	mov [ebp-4], edx

	popad

	mov eax,[ebp-4]
	mov esp,ebp
	pop ebp
	ret


section .rodata
fmt db `int:  %d. \n`, 0