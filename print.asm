section .text
GLOBAL print
extern strlen
extern exit
; Funcion print:
; pushear al stack en orden: 
;  - length
;  - string
; 
; 
;Example:  
;_start:
;	push length
;	push string
;	call _print
;	mov eax,0x01
;	int 80h

print:
; Levanto length y string a edx y ecx y devuelvo el return address
	push ebp
	mov ebp,esp
	pushad
	
	mov ecx, [ebp+8]

	push ecx
	call strlen
	add esp,4
	mov edx,eax
; Imprimo
	mov eax,4
	mov ebx, 1
	int 80h

	mov edx,1
	mov ecx,space
	mov eax,4
	mov ebx,1
	int 80h

	popad

	mov esp,ebp
	pop ebp
	ret
	
section .data
space db 32