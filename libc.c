#define STDOUT 1

int sys_write(int fd, void *buffer, int size);
int strlen(char* str);
unsigned int pid();

int puts(char* str) {
	int mpid = pid();
    int len = strlen(str);
     
     return sys_write(STDOUT, (void *) str, len);
}